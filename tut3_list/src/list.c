#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/list.h"

node* list_node_alloc( void ){ 
    node* patient = (node*) malloc (sizeof(node));
    return patient;
}

node* list_add( int id, char *name ){
    node* patient = list_node_alloc();
    
    patient->id = id;
    strncpy( patient->name, name, 10 );
    patient->next = NULL;

    if( head == NULL ) {
       head = tail = patient;
    } else {
       tail->next = patient;
       tail = patient;  
    }

    return patient;
}

void list_remove() {
    // assuming that we will remove always remove first node
    if( head == NULL ) return; // empty list

    if( head->next == NULL){ // head is the only node
        free(head);
        head = tail = NULL; 
    } else { // the list has more than one node
        node *tmp = head->next;
        free(head);
        head=tmp;        
    }
}

void list_print(){
    printf("\nPrinting List");
    printf("\n");
    for ( node *tmp=head; tmp!= NULL; tmp=tmp->next ){
        printf("(%d, %s)-->", tmp->id, tmp->name );
    }
    printf("NULL");
    printf("\n===================================");
}
