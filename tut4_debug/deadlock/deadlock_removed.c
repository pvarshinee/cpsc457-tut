#include <stdio.h> 
#include <pthread.h> 
#include <stdlib.h>
#include <unistd.h>

pthread_mutex_t read_mutex;
pthread_mutex_t write_mutex;

void * write_file(void *temp) {
	char *ret;
	FILE *file1;
	char *str;

	pthread_mutex_lock(&write_mutex);
	printf("\nwrite_file: write_lock acquired \n");
	pthread_mutex_lock(&read_mutex);
	printf("\nwrite_file: read_lock acquired \n");

	printf("\nFile locked, please enter the message \n");
	str=(char *)malloc(10*sizeof(char));
	file1=fopen("temp","w");
	scanf("%s",str);
	fprintf(file1,"%s",str);
	fclose(file1);

	pthread_mutex_unlock(&read_mutex);
	pthread_mutex_unlock(&write_mutex);
	printf("\nwrite_file: Unlocked both the locks \n");
	return ret;
}


void * read_file(void *temp) {

	char *ret;
	FILE *file1;
	char *str;

read:

	pthread_mutex_lock(&write_mutex);
	printf("\nread_file: write_lock acquired\n");
	pthread_mutex_lock(&read_mutex);
	printf("\nread_file: read_lock acquired\n");

	printf("\n Opening file \n");
	file1=fopen("temp","r");
	str=(char *)malloc(10*sizeof(char));
	fscanf(file1,"%s",str);

	if( str != NULL ) 
		printf("\n Message from file is %s \n", str);

	fclose(file1);

	pthread_mutex_unlock(&read_mutex);
	pthread_mutex_unlock(&write_mutex);
	printf("\nread_file: Unlocked both the locks \n");

	if( str == NULL ) goto read;

	return ret;
}

int main() {
	pthread_t thread_id,thread_id1;
	pthread_attr_t attr;

	int ret;
	void *res;

	ret=pthread_create(&thread_id,NULL,&write_file,NULL);
	ret=pthread_create(&thread_id1,NULL,&read_file,NULL);
	printf("\n Created thread");

	pthread_join(thread_id,&res);
	pthread_join(thread_id1,&res);

	return 0;
}
