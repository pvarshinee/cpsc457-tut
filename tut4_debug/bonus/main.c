/*
 *  * This program displays the names of all files in the current directory.
 *   */

#include <dirent.h> 
#include <stdio.h> 

int main( int argc, char *argv[] )
{
  DIR    *d;
  struct dirent *dir;

  if( argc != 2 ){
    printf("Please mention file or directory name\n");
    return 0;
  }

  int found = 0; // flag variable
  d = opendir(".");
  if (d)
  {
    while ((dir = readdir(d)) != NULL){
       if (strcmp( argv[1], dir->d_name ) == 0 ){

          found = 1;
          // we found the file or directory
          if( dir->d_type == DT_DIR ){
            printf("\n%s is a directory\n", dir->d_name );             
            break;
          }      
          else if( dir->d_type == DT_REG ){
            printf("\n%s is a file\n", dir->d_name);
            break;
          }
          else{
	    printf("\n%s is neither a file nor a directory\n\n");
	  }
       }
    }

    if( found == 0 ) {
       printf("\n%s:No such file or directory\n", argv[1]);
    }

    printf("\n");
    closedir(d);
  }

  return(0);
}
