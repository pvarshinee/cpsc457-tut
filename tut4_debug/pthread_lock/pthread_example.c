#include <stdio.h>
#include <pthread.h>


int increment_me=0;

void* increment( void *arg )
{
	for( int i=0; i<2000000; i++ ){
		increment_me++;
	}
	return NULL;
}

int main(){

	pthread_t thread[2];
	for( int i=0; i<2; i++ ){
		pthread_create( &thread[i], NULL, increment, NULL );
	}
	pthread_join( thread[0], NULL );
	pthread_join( thread[1], NULL );

	printf("\n\nThe value of increment_me is %d\n\n", increment_me );
	return 0;
}
