#ifndef LIST_H
#include "stdlib.h"
#define LIST_H 1

    typedef struct patient{
        int id;
        char name[10];
        struct patient *next
    }node;
    
    extern node *head;
    extern node *tail;

    node* list_node_alloc( void );
    node* list_add( int id, char *name);
    void  list_remove();
    void  list_print();
#endif
