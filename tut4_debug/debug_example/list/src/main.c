
#include "../include/list.h"

// using list function
int main(){

    // print initial empty list
    list_print();

    // list with 3 people    
    list_add( 1, "James" );
    list_add( 2, "Leslie" );
    list_add( 3, "Mickey");
    list_print();

    // list remove
    list_remove();
    list_print();

    //printing empty list
    list_remove();
    list_remove();
    list_print();
}
