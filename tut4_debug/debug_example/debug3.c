#include<stdio.h>
#include<ctype.h>
#include<string.h>

int are_anagrams (const char *word1, const char *word2);

int main (void)
{
    char an1[30], an2[30];
    int j;
    printf("Enter first word: ");
    scanf("%s", an1);
    printf("Enter second word: ");
    scanf("%s", an2);
    printf("The words are");

    j = are_anagrams (an1, an2);

    if (j == 0)
    {
        printf(" not anagrams. \n");
    }else
        printf(" anagrams. \n");

    return 0;
}

int are_anagrams (const char *word1, const char *word2)
{
    int i;
    int check[26] = {0};
    for(i=0; i<30; i++)
        if(word1[i] == '\0')
            i=40;
        else {
            word1[i] = toupper(word1[i]);
            check[word1[i]-65]++;
        }

    	  for(i=0; i<30; i++)
        		if(word2[i] == '\0')
            	i=40;
        		else {
            	word2[i] = toupper(word2[i]);
            	check[word2[i]-65]--;
        		}

 	     for(i=0; i<26; i++)
     		   if(check[i] != 0)
            	return 0;
    return 1;
}
