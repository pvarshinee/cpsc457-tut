#include <stdio.h>

int main(){
	int number1, number2;
	double quotient;

	printf(" If you enter two numbers, \
					I would tell you the quotient \n");
	printf(" Number 1: ");
	scanf("%d", &number1 );
	printf(" Number 2: ");
	scanf("%d", number2);

	quotient = number1 / number2;

	printf(" %lf  is the quotient\n\n", quotient );
}


// try the program for numbers like 5,3 where number1 is not multiple of number 2
