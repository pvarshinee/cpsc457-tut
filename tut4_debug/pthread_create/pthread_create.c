#include <stdio.h>
#include <pthread.h>


void* increment( void *arg )
{	
	printf("\n  Hello  %s", (char*)arg );
	return NULL;
}

int main(){

        void *ptr;
	int   int_ptr[3]={1,2,3}; 
	ptr = &int_ptr[0];

//ptr = ptr+1;

	printf("\n%d", *((int*)ptr) );

	pthread_t thread[2];

	char name[50];
	printf("What is your name? ");
	scanf("%s", name );

	for( int i=0; i<2; i++ ){
		pthread_create( &thread[i], NULL, increment, (void*)name );
	}
	pthread_join( thread[0], NULL );
	pthread_join( thread[1], NULL );

	printf("\n\n");

	return 0;
}
