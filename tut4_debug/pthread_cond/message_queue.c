/* Implementation of message queue routines */
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "message_queue.h"

int queue_full=0;
unsigned long queue_empty=0;

// we insert at the tal and increment the index
void message_queue_insert( Message_queue_struct *queue, Message *message )
{
   pthread_mutex_lock( &(queue->condition_mutex ) );

   if ( queue->unclaimedElements == MAX_QUEUE_SIZE )
   { 
      queue->queue_full_waiting++;
      pthread_cond_wait( &(queue->condition_variable_client), &(queue->condition_mutex) ); 
      queue->unclaimedElements--;
   }
   
   // add to the queue
   queue->message[queue->tail] = message;

   // update counters
   queue->tail = (queue->tail+1) & (MAX_QUEUE_SIZE-1);
   queue->size++;
   queue->unclaimedElements++;
    
   // check if the queue had been previously empty
   if( queue->size == 1 ) {
      pthread_cond_signal( &(queue->condition_variable_server) );   
   }
      
   pthread_mutex_unlock( &(queue->condition_mutex ) ); 
   return;   
}

/*------------------------------------------------------------------*/
// remove at head and increment the index
Message* message_queue_remove( Message_queue_struct *queue, Message *m )
{
   pthread_mutex_lock( &(queue->condition_mutex ) );

   // queue empty
   if( queue->size == 0 ) {
      pthread_cond_wait( &(queue->condition_variable_server), &(queue->condition_mutex) );
   }

   // copy the message from the queue from queue
   Message* data =  queue->message[queue->head];

   // update counters
   queue->head = (queue->head+1) &(MAX_QUEUE_SIZE-1); 
   queue->size--;

   // if anyones waiting to be signalled
   if( queue->queue_full_waiting > 0 ) {  
       queue->queue_full_waiting--; 
       pthread_cond_signal( &(queue->condition_variable_client) );      
       //unclaimed elements will be deceremented by the thread that woke up
   }
   else {
      queue->unclaimedElements--; // baton 
   }
   
   //unlock
   pthread_mutex_unlock( &(queue->condition_mutex ) );
   return data; 
}

/*------------------------------------------------------------------*/
void message_queue_init( Message_queue_struct *queue )
{  
   // intializinf indexes, signal variables, counters
   queue->head = 0;
   queue->tail = queue->unclaimedElements = queue->size = 0;   

   queue->queue_full_waiting = queue->queue_empty_waiting = 0;

   // intializing synchronization variables   
   pthread_mutex_init( &(queue->condition_mutex), NULL );
   pthread_cond_init ( &(queue->condition_variable_server), NULL );
   pthread_cond_init ( &(queue->condition_variable_client), NULL );  

   // initializing queue
   int index=0;
   for ( index=0; index<MAX_QUEUE_SIZE; index++ )
   {
      queue->message[index] = NULL;
   }  
}
