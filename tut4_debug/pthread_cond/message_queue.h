#ifndef MESSAGE_QUEUE_H
#define MESSAGE_QUEUE_H 1

/* definition for Queues */
#include <pthread.h>
#include "memcached.h"

#define MAX_QUEUE_SIZE 4096

typedef struct{
   item **result; __attribute__((aligned(32)))
   uint32_t      hv;
   size_t        ntotal;
   unsigned int  id;
   uint64_t      *evicted;
   char          *slabs;
   int           *asynchronous_return_value_ptr;
   int           *no_go_ptr;
   unsigned int  *total_pages;    
   char          **ret_ptr;    
   void          *new_c;
   ADD_STAT      add_stats;

   pthread_mutex_t *sync_mutex;
   pthread_cond_t  *sync_cond;

}Message;

extern int mq_fd;

// queue structure
typedef struct{
   int      head; __attribute__((aligned(64)))
   unsigned unclaimedElements;
   Message  *message[MAX_QUEUE_SIZE]; 
   
   int queue_full_waiting;
   int queue_empty_waiting;
   
   //synchronization variables
   pthread_mutex_t      condition_mutex;
   pthread_cond_t       condition_variable_client;
   pthread_cond_t       condition_variable_server;
   
   // flag variable
   int  tail;
   int  size;
}Message_queue_struct;

//queue declaration
extern Message_queue_struct async_queue;

//function declarations
void     message_queue_insert( Message_queue_struct *queue, Message *message );
Message* message_queue_remove( Message_queue_struct *queue, Message *recv_msg );
void     message_queue_init( Message_queue_struct *queue );

void message_queue_print_fullempty();

#endif
